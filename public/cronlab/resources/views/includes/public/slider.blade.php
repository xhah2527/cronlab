<!--     *********     HEADER 3      *********      -->
<div class="carousel-inner">
    <div class="item active">
        <div class="page-header header-filter" style="background-image: url('/img/dg1.jpg');">

            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-left">
                        <h1 class="title">Welcome to {{config('app.name')}}</h1>
                        <h4>You can get $ 5.55 free bonus right now just register with ROIForex Fund with no deposit at www.roiforex.club </h4>
                        <br />

                        <div class="buttons">

                            @if(Auth::guest())

                            <a href="{{ route('register') }}" class="btn btn-primary btn-lg">
                                Register Now
                            </a>

                            @else

                                <a href="" class="btn btn-info btn-lg">
                                   View Ads Now
                                </a>

                            @endif
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="item">
        <div class="page-header header-filter" style="background-image: url('/img/dg2.jpg');">

            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-left">
                        <h1 class="title">High Income with Low Investment Risk</h1>
                        <h4>A simple and affordable way to increase your wealth by investing a small sum of money. Profit is calculated on daily and weekly configurations and reinvested to increase your principal deposit and the overall profit. Profits and principal deposit are paid out at the end of the investment period.</h4>
                        <br />
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class="item">
        <div class="page-header header-filter" style="background-image: url('img/dg3.jpg');">

            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-left">
                        <h1 class="title">Up To 10 Level Referral Comission</h1>
                        <h4>You can also receive additional income on our website using our lucrative referral program. For every user signed up under your referral link, you will receive 5% to 10% of whatever they deposit.</h4>
                        <br />

                    </div>
                </div>
            </div>

        </div>

    </div>

</div>