<div class="features-1">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h2 class="title">Why We Are The Best?</h2>
            <h5 class="description">ROIFOREX is The combination of super global traders from around the world. The main purpose is to stop the damage from the foreign exchange market (Forex) and provide investors with stable profit returns. Invest in the most valuable market in the world and make the most profit.  Start Earning Profit With Safety NOW!</h5>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="info">
                <div class="icon icon-primary">
                    <i class="material-icons">lock</i>
                </div>
                <h4 class="info-title">Security</h4>
                <p>Our website resides on a dedicated server with DDoS protection and SSL certificates, so attacks are virtually impossible. You can access your personal dashboard anytime, anywhere – 24/7.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info">
                <div class="icon icon-info">
                    <i class="material-icons">directions_bike</i>
                </div>
                <h4 class="info-title">Easy To Use</h4>
                <p>Just a few click your order is ready. Once you deposit, your coin starts to multiply right away. It is designed to be user friendly and easy to operate for both professionals and new investors.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info">
                <div class="icon icon-primary">
                    <i class="material-icons">local_atm</i>
                </div>
                <h4 class="info-title">Professional</h4>
                <p>Seize the opportunity to grow your capital in the most liquid market in the world by copying the trades of top preforming traders in our investment programme.</p>
            </div>
        </div>
    </div>

</div>