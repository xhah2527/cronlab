

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 text-center">
            <h2 class="title">Choose your desire Plan to invest</h2>
            <h5 class="description">ROIFOREX [Returns On Investment Forex] offers a variety of investment options for investors. You can choose the amount of funds, returns and time as needed. Start Earning Profit with safety NOW!</h5>
            <div class="section-space"></div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-4 ">
            <div class="card card-pricing card-raised">
                <div class="card-content content-primary">
                    <h6 class="category">Starters</h6>
                    <h1 class="card-title"><small>{{config('app.currency_symbol')}} </small><small>from</small>50<small>.00</small></h1>
                    <ul>
                        <li><b>117%</b> Profit Return</li>
                        <li>Deposit Included In Payment</li>
                        <li>Reinvestment Available</li>
                        <li>Daily For 30 Days </li>
                    </ul>

                    @if(Auth::guest())
                    <a href="{{route('register')}}" class="btn btn-white btn-round">
                        Get Started
                    </a>
                    @else
                        <a href="" class="btn btn-white btn-round">
                            Get Started
                        </a>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card card-pricing card-raised">
                <div class="card-content content-info">
                    <h6 class="category text-info">Platinum</h6>
                    <h1 class="card-title"><small>{{config('app.currency_symbol')}} </small><small>from</small>100<small>.00</small></h1>
                    <ul>
                        <li><b>225%</b> Profit Return</li>
                        <li>Deposit Included In Payment</li>
                        <li>Reinvestment Available</li>
                        <li>Daily For 30 Days </li>
                    </ul>
                    @if(Auth::guest())
                        <a href="{{route('register')}}" class="btn btn-white btn-round">
                            Get Started
                        </a>
                    @else
                        <a href="" class="btn btn-white btn-round">
                            Get Started
                        </a>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-pricing card-raised">
                <div class="card-content content-success">
                    <h6 class="category text-info">Diamond</h6>
                    <h1 class="card-title"><small>{{config('app.currency_symbol')}} </small><small>from</small>1500<small>.00</small></h1>
                    <ul>
                        <li><b>457%</b> Profit Return</li>
                        <li>Deposit Included In Payment</li>
                        <li>Reinvestment Available</li>
                        <li>Daily For 180 Days </li>
                    </ul>
                    @if(Auth::guest())
                        <a href="{{route('register')}}" class="btn btn-white btn-round">
                            Get Started
                        </a>
                    @else
                        <a href="" class="btn btn-white btn-round">
                            Get Started
                        </a>
                    @endif
                </div>
            </div>
        </div>

    </div>

</div>